#include "pfs0.h"

#include <stdlib.h>
#include <string.h>
#include <assert.h>

int_fast64_t pfs0_parse(bae_file_entry_t** output_, baecb_read input, void* cbdata, uint_fast64_t offset) {
	int_fast64_t ret;
	
	pfs0_header_t header;
	pfs0_file_entry_t* pfs_entry_table;
	char* pfs_stringtable;
	
	// Header
	(*input)(&header, offset, sizeof(pfs0_header_t), cbdata);
	ret = header.filecount;
	
	// Entry table
	pfs_entry_table = malloc( sizeof(pfs0_file_entry_t) * header.filecount );
	assert(pfs_entry_table != NULL);
	(*input)(pfs_entry_table, offset + sizeof(pfs0_header_t), sizeof(pfs0_file_entry_t) * header.filecount, cbdata);
	
	// String table
	uint_fast64_t stringtable_offset = offset + sizeof(pfs0_header_t) + sizeof(pfs0_file_entry_t)*header.filecount;
	pfs_stringtable = malloc( header.stringtable_size );
	assert(pfs_stringtable != NULL);
	(*input)(pfs_stringtable, stringtable_offset, header.stringtable_size, cbdata);
	
	uint_fast64_t data_offset = stringtable_offset + header.stringtable_size;
	
	*output_ = malloc( sizeof(bae_file_entry_t) * header.filecount );
	bae_file_entry_t* output = *output_;
	assert(output != NULL);
	
	for(uint32_t i=0; i<header.filecount; i++) {
		pfs0_file_entry_t* pfs_e = &pfs_entry_table[i];
		output[i].filename = strdup( &pfs_stringtable[ pfs_e->name_offset ] );
		assert(output[i].filename != NULL);
		output[i].offset = data_offset + pfs_e->offset;
		output[i].size = pfs_e->filesize;
	}
	
	free(pfs_stringtable);
	free(pfs_entry_table);
	
	return ret;

}
