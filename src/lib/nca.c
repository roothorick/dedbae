#include "nca.h"

#include "aes.h"
#include "switchkeys.h"
#include "utils.h"
#include "types.h"
#include "cnmt.h"

#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#define MAX_WORKBUFFER_SIZE 0x40000000

static uint_fast8_t key_area_index(nca_header_t* header) {
	uint_fast8_t ret;
	if(header->crypto_type1 < header->crypto_type2)
		ret = header->crypto_type2;
	else
		ret = header->crypto_type1;
	
	// I... *think* that 3.x and later crypto actually start with key 2, not 3. And I guess key 1 doesn't exist.
	// In any case, this is what everyone else is doing and it produces correct output.
	if(ret > 0) ret--;
	
	return ret;
}

// It appears the CTR is simply the section's real offset shifted four bits, like so:
// update_ctr taken from hactool © SciresM
static void update_ctr(unsigned char *ctr, uint_fast64_t ofs) {
	ofs >>= 4;
	for (unsigned int j = 0; j < 0x8; j++) {
		ctr[0x10-j-1] = (unsigned char)(ofs & 0xFF);
		ofs >>= 8;
	}
}

static void decrypt_key_area(nca_header_t* header, char filekeys[4][16]) {
	// We do not write decrypted keys to the header struct,
	// so as to make compatibility with hactool easier.
	uint_fast8_t key_idx = key_area_index(header);
	
	aes_ctx_t* aes_ctx = new_aes_ctx(switchkeys.application_key_area[key_idx], 16, AES_MODE_ECB);
	assert(aes_ctx != NULL);
	aes_decrypt(aes_ctx, filekeys, header->keys, 16*4);
	free_aes_ctx(aes_ctx);
}

static void decrypt_titlekey(nca_header_t* header, char* out, char* titlekey) {
	// Find key index
	uint_fast8_t key_idx;
	if(header->crypto_type1 < header->crypto_type2)
		key_idx = header->crypto_type2;
	else
		key_idx = header -> crypto_type1;
	
	// Apparently key area key 0 never really existed or something?
	if(key_idx > 0)
		key_idx--;
	
	aes_ctx_t* aes_ctx = new_aes_ctx(switchkeys.titlekeks[key_idx], 16, AES_MODE_ECB);
	assert(aes_ctx != NULL);
	aes_decrypt(aes_ctx, out, titlekey, 16);
	free_aes_ctx(aes_ctx);
}

void nca_generate_ncaid(char* id, baecb_read input, void* cbdata, uint_fast64_t size) {
	unsigned char hash[32];
	
	hash_file_256(hash, input, cbdata, size);
	
	for(uint_fast8_t i=0; i<16; i++)
		sprintf(id + i*2, "%02x", hash[i]);
}

uint8_t nca_content_type_from_cnmt(uint8_t flag) {
	switch(flag) {
		case CNMT_CONTENT_TYPE_META:
			return NCA_CONTENT_TYPE_META;
		case CNMT_CONTENT_TYPE_PROGRAM:
			return NCA_CONTENT_TYPE_PROGRAM;
		case CNMT_CONTENT_TYPE_DATA:
			return NCA_CONTENT_TYPE_DATA;
		case CNMT_CONTENT_TYPE_CONTROL:
			return NCA_CONTENT_TYPE_CONTROL;
		case CNMT_CONTENT_TYPE_MANUAL:
			return NCA_CONTENT_TYPE_MANUAL;
		case CNMT_CONTENT_TYPE_LEGALINFO:
			return NCA_CONTENT_TYPE_MANUAL; // XXX: Blind guess
		case CNMT_CONTENT_TYPE_GAME_UPDATE:
			return NCA_CONTENT_TYPE_DATA; // XXX: Blind guess
	}
}

bool nca_has_rightsid(nca_header_t* header) {
	static char zeroes[16]; // Implicitly zero-filled because static
	if( memcmp( zeroes, header->rights_id, 16) != 0 )
		return true;
  
	return false;
}

void nca_init_header(nca_header_t* hd, nca_header_t* old_hd) {
	memset(hd, 0, sizeof(nca_header_t));
	strncpy(hd->magic, "NCA3", 4);
	
	if( old_hd != NULL ) {
		
		hd->location = old_hd->location;
		hd->content_type = old_hd->content_type;
		hd->crypto_type1 = old_hd->crypto_type1;
		hd->key_index = old_hd->key_index;
		hd->titleid = old_hd->titleid;
		hd->sdk_version = old_hd->sdk_version;
		hd->crypto_type2 = old_hd->crypto_type2;
		memcpy(hd->rights_id, old_hd->rights_id, 16);
		memcpy(hd->keys, old_hd->keys, 4*16);
	}
	else {
		hd->location = NCA_LOCATION_CONSOLE;
		hd->content_type = NCA_CONTENT_TYPE_PROGRAM;
		hd->crypto_type1 = NCA_CRYPTO_TYPE1_PRE_3;
		hd->key_index = 0;
		// TODO: title ID?
		hd->sdk_version = 0x1020000; // Value from most launch titles
		hd->crypto_type2 = 0;
	}
}

void nca_decrypt_header(nca_header_t* out, char* in) {
	aes_ctx_t* aes_ctx = new_aes_ctx(switchkeys.header, sizeof(switchkeys.header), AES_MODE_XTS);
	assert(aes_ctx != NULL);
	aes_xts_decrypt(aes_ctx, out, in, sizeof(nca_header_t), 0, 0x200);
	free_aes_ctx(aes_ctx);
}

void nca_encrypt_header(char* out, nca_header_t* in) {
	// Finalize by updating hashes
	for(uint_fast8_t i=0; i<4; i++)
		if(in->section_table[i].media_offset != 0)
			hash_buf_256( &in->section_headers[i], in->hashes[i], sizeof(nca_section_header_t) );
	
	aes_ctx_t* aes_ctx = new_aes_ctx(switchkeys.header, sizeof(switchkeys.header), AES_MODE_XTS);
	assert(aes_ctx != NULL);
	aes_xts_encrypt(aes_ctx, out, in, sizeof(nca_header_t), 0, 0x200);
	free_aes_ctx(aes_ctx);
}

uint_fast64_t nca_offsetof_section(nca_header_t* header, uint_fast8_t section) {
	return header->section_table[section].media_offset * 0x200ULL;
}

static aes_ctx_t* make_section_crypto_ctx(nca_header_t* header, uint_fast8_t section, char* titlekey) {
	char filekeys[4][16];
	char iv[16];
	
	if(header->section_headers[section].cryptotype == NCA_SECTION_CRYPTO_TYPE_PLAINTEXT)
		return NULL;
	
	if( titlekey == NULL || ! nca_has_rightsid(header) )
		decrypt_key_area(header, filekeys);
	else
		// Cheating a little :P
		decrypt_titlekey(header, filekeys[2], titlekey);
	
	memset(iv, 0x0, 16);
	
	// Read portion of CTR from section header
	// Byte swapping because good ol' LSB
	for(uint_fast8_t i=0; i<8; i++) {
		int j=7-i;
		iv[i] = header->section_headers[section].section_ctr[j];
	}

	// Keys 0, 1, and 3 are always the same and usually zeroes.
	// Also, hactool hardcodes to 2. Hmm.
	aes_ctx_t* ret = new_aes_ctx(filekeys[2], 16, AES_MODE_CTR);
	assert(ret != NULL);
	update_ctr(iv, header->section_table[section].media_offset*0x200);
	aes_setiv(ret, iv, 16);
	
	return ret;
}

void nca_decrypt_section(nca_header_t* header, uint_fast8_t section, char* out, char* in, uint_fast64_t size, char* titlekey) {
	aes_ctx_t* ctx = make_section_crypto_ctx(header, section, titlekey);
	aes_decrypt(ctx, out, in, size);
	// This will be null if the section is plaintext
	if(ctx != NULL)
		free_aes_ctx(ctx);
}

void nca_encrypt_section(nca_header_t* header, uint_fast8_t section, char* out, char* in, uint_fast64_t size, char* titlekey) {
	aes_ctx_t* ctx = make_section_crypto_ctx(header, section, titlekey);
	aes_encrypt(ctx, out, in, size);
	// This will be null if the section is plaintext
	if(ctx != NULL)
		free_aes_ctx(ctx);
}

// XXX So many arguments! Is there a cleaner way?
static void copy_with_crypto(aes_ctx_t* aes_ctx, uint_fast64_t size,
		baecb_write out, void* outcbdata, uint_fast64_t out_offset,
		baecb_read in, void* incbdata, uint_fast64_t in_offset,
		bool encrypt) {
	char inbuf[0x200];
	char outbuf[0x200];
	
	for(uint64_t i=0; i<size; i+= 0x200) {
		if( i+0x200 > size ) {
			memset(inbuf, 0, 0x200);
			(*in)(inbuf, i + in_offset, size - i, incbdata);
		}
		else
			(*in)(inbuf, i + in_offset, 0x200, incbdata);
		
		if(aes_ctx == NULL)
			memcpy(outbuf, inbuf, 0x200);
		else {
			if(encrypt)
				aes_encrypt(aes_ctx, &outbuf, &inbuf, 0x200);
			else
				aes_decrypt(aes_ctx, &outbuf, &inbuf, 0x200);
		}
		(*out)(outbuf, i + out_offset, 0x200, outcbdata);
	}
}

void nca_extract_section(nca_header_t* header, uint_fast8_t section,
		baecb_write out, void* outcbdata,
		baecb_read in, void* incbdata, char* titlekey) {
	
	aes_ctx_t* aes_ctx = make_section_crypto_ctx(header, section, titlekey);
	copy_with_crypto(aes_ctx, nca_sizeof_section(header, section),
			out, outcbdata, 0,
			in, incbdata, nca_offsetof_section(header, section),
			false);
	
	// This will be null if the section is plaintext
	if(aes_ctx != NULL)
		free_aes_ctx(aes_ctx);
}

void nca_replace_section(nca_header_t* header, uint_fast8_t section,
		baecb_write out, void* outcbdata,
		baecb_read in, void* incbdata, char* titlekey) {
	
	aes_ctx_t* aes_ctx = make_section_crypto_ctx(header, section, titlekey);
	copy_with_crypto(aes_ctx, nca_sizeof_section(header, section),
			out, outcbdata, nca_offsetof_section(header, section),
			in, incbdata, 0,
			true);
	
	// This will be null if the section is plaintext
	if(aes_ctx != NULL)
		free_aes_ctx(aes_ctx);
}

static void make_hashtable(char* out, baecb_read in, void* cbdata, uint_fast64_t blocksize, uint_fast64_t filesize, bool use_padding) {
	uint_fast64_t tablesize = ceil(filesize / (float) blocksize) * 32;
	
	char* inbuf = malloc(blocksize);
	assert(inbuf != NULL);
	
	uint_fast64_t i=0;
	for(; (i+1)*32 < tablesize; i++) {
		(*in)(inbuf, i*blocksize, blocksize, cbdata);
		hash_buf_256(inbuf, out + i*32, blocksize);
	}
	// Last hash in the table
	if(i*32 < tablesize) {
		unsigned int last_block_size = filesize - i*blocksize;
		if(use_padding)
			memset(inbuf, 0, blocksize);
			
		(*in)(inbuf, i*blocksize, last_block_size, cbdata);
		
		if(use_padding)
			hash_buf_256(inbuf, out + i*32, blocksize);
		else
			hash_buf_256(inbuf, out + i*32, last_block_size);
	}
	
	free(inbuf);
}

void nca_inject_pfs0(nca_header_t* header, uint_fast8_t section,
		baecb_write out, void* outcbdata,
		baecb_read in, void* incbdata, uint_fast64_t fs_size, char* titlekey) {
	nca_section_header_t* sec_hd = &header->section_headers[section];
	
	// Collect some starting values
	uint_fast64_t blocksize = sec_hd->pfs0.blocksize;
	if(blocksize == 0)
		// TODO: Intelligently choose blocksize based on... dunno, some criteria
		blocksize = 0x1000;
	// One 32 byte hash per 0x1000 bytes
	uint_fast64_t hashtable_size = 32 * ceil(fs_size / (float) blocksize);
	
	// Fill in parts of the section header/superblock we already know
	sec_hd->partition_type = NCA_PARTITION_TYPE_PFS0;
	sec_hd->fstype = NCA_FSTYPE_PFS0;
	if( sec_hd->cryptotype == 0 )
		sec_hd->cryptotype = NCA_SECTION_CRYPTO_TYPE_PLAINTEXT;
	
	sec_hd->pfs0.blocksize = blocksize;
	sec_hd->pfs0.unknown_0 = 0x2;
	sec_hd->pfs0.hashtable_offset = 0x0;
	sec_hd->pfs0.hashtable_size = hashtable_size;
	if( sec_hd->pfs0.header_offset == 0 )
		sec_hd->pfs0.header_offset = hashtable_size;
	sec_hd->pfs0.fs_size = fs_size;
	
	// Calculate the penultimate section size
	uint32_t section_mediasize = ceil( (fs_size + hashtable_size) / (float) 0x200 );
	
	if(section < 1)
		header->section_table[section].media_offset = 6; // Right at the end of the NCA header
	else
		header->section_table[section].media_offset = header->section_table[section-1].media_end;
	header->section_table[section].media_end = header->section_table[section].media_offset + section_mediasize;
	
	if(hashtable_size > MAX_WORKBUFFER_SIZE)
		//bail("Absurdly large file! Are you SURE that's correct?");
		// TODO: Return error
		return;
	
	char* hashtable = malloc( ceil(fs_size / (float) blocksize) * 32 );
	assert(hashtable != NULL);
	make_hashtable(hashtable, in, incbdata, blocksize, fs_size, false);
	
	// Master hash
	hash_buf_256(hashtable, sec_hd->pfs0.master_hash, hashtable_size);
	
	aes_ctx_t* aes_ctx = make_section_crypto_ctx(header, section, titlekey);
	
	// Encrypt and write hashtable
	char* outbuf = malloc(hashtable_size);
	assert(outbuf != NULL);
	if(aes_ctx == NULL)
		memcpy(outbuf, hashtable, hashtable_size);
	else
		aes_encrypt(aes_ctx, outbuf, hashtable, hashtable_size);
	
	free(hashtable);
	
	(*out)(outbuf, nca_offsetof_section(header, section), hashtable_size, outcbdata);
	free(outbuf);
	
	// Encrypt and write the PFS0 itself
	uint64_t out_offset = nca_offsetof_section(header, section) + hashtable_size;
	copy_with_crypto(aes_ctx, fs_size, out, outcbdata, out_offset, in, incbdata, 0, true);
	
	// This will be null if the section is plaintext
	if(aes_ctx != NULL)
		free_aes_ctx(aes_ctx);
	
	// Header hash
	hash_buf_256(sec_hd, header->hashes[section], sizeof(nca_section_header_t));
}

void nca_inject_romfs(nca_header_t* header, uint_fast8_t section,
		baecb_write out, void* outcbdata,
		baecb_read in, void* incbdata, uint_fast64_t fs_size, char* titlekey) {
	nca_section_header_t* sec_hd = &header->section_headers[section];
	
	// Fill in parts of the section header/superblock we already know
	sec_hd->partition_type = NCA_PARTITION_TYPE_ROMFS;
	sec_hd->fstype = NCA_FSTYPE_ROMFS;
	if( sec_hd->cryptotype == 0 )
		sec_hd->cryptotype = NCA_SECTION_CRYPTO_TYPE_PLAINTEXT;
	
	memcpy(sec_hd->romfs.magic, "IVFC", 4);
	sec_hd->romfs.magic2 = 0x20000;
	sec_hd->romfs.master_hash_size = 0x20;
	sec_hd->romfs.unknown_1 = 0x7;
	
	// Set a default blocksize
	// TODO: Intelligently choose blocksize based on... dunno, some criteria
	for(uint_fast8_t i=0; i<6; i++)
		if( sec_hd->romfs.levels[i].blocksize == 0 )
			sec_hd->romfs.levels[i].blocksize = 0xe; // log2(0x4000)

#define ACTUAL_BLOCKSIZE(level) powf(2, sec_hd->romfs.levels[level].blocksize)

	// Calculate hashtable sizes
	sec_hd->romfs.levels[5].size = fs_size;
	for(int_fast8_t i=4; i>=0; i--) {
		// Breaking this down because the calculation is a little complex
		uint_fast64_t num_hashes = ceil( sec_hd->romfs.levels[i+1].size / ACTUAL_BLOCKSIZE(i) );
		uint_fast64_t actual_hashtable_size = num_hashes * 32;
		sec_hd->romfs.levels[i].size = ceil( actual_hashtable_size / ACTUAL_BLOCKSIZE(i) ) * ACTUAL_BLOCKSIZE(i);
	}
	
	// Offsets
	sec_hd->romfs.levels[0].offset = 0;
	for(uint_fast8_t i=1; i<6; i++)
		sec_hd->romfs.levels[i].offset = sec_hd->romfs.levels[i-1].offset + sec_hd->romfs.levels[i-1].size;
	
	// Calculate the penultimate section size
	uint32_t section_mediasize = ceil( (sec_hd->romfs.levels[5].offset + fs_size) / (float) 0x200 );
	
	if(section < 1)
		header->section_table[section].media_offset = 6; // Right at the end of the NCA header
	else
		header->section_table[section].media_offset = header->section_table[section-1].media_end;
	header->section_table[section].media_end = header->section_table[section].media_offset + section_mediasize;
	
	// Hashtables!
	char* hashtables[5];
	hashtables[4] = malloc( sec_hd->romfs.levels[4].size );
	assert(hashtables[4] != NULL);
	memset(hashtables[4], 0x00, sec_hd->romfs.levels[4].size);
	make_hashtable(hashtables[4], in, incbdata, ACTUAL_BLOCKSIZE(5), fs_size, true);
	for(int_fast8_t i=3; i>=0; i--) {
		hashtables[i] = malloc( sec_hd->romfs.levels[i].size );
		assert(hashtables[i] != NULL);
		memset(hashtables[i], 0x00, sec_hd->romfs.levels[i].size);
		make_hashtable(hashtables[i], &baecb_read_memcpy, hashtables[i+1], ACTUAL_BLOCKSIZE(i+1), sec_hd->romfs.levels[i+1].size, true);
	}
	
	// Hash in the superblock
	hash_buf_256(hashtables[0], sec_hd->romfs.hash, sec_hd->romfs.levels[0].size);
	
	// Ready to write to file
	aes_ctx_t* aes_ctx = make_section_crypto_ctx(header, section, titlekey);
	
	// Writing hashtables
	for(uint_fast8_t i=0; i<5; i++) {
		copy_with_crypto(aes_ctx, sec_hd->romfs.levels[i].size,
				out, outcbdata, nca_offsetof_section(header, section) + sec_hd->romfs.levels[i].offset,
				&baecb_read_memcpy, hashtables[i], 0,
				true);
		free(hashtables[i]);
	}
	
	// Writing the file itself
	copy_with_crypto(aes_ctx, fs_size,
			out, outcbdata, nca_offsetof_section(header, section) + sec_hd->romfs.levels[5].offset,
			in, incbdata, 0,
			true);
	
	// This will be null if the section is plaintext
	if(aes_ctx != NULL)
		free_aes_ctx(aes_ctx);
#undef ACTUAL_BLOCKSIZE
}

uint_fast64_t nca_sizeof_section(nca_header_t* header, uint_fast8_t section) {
	return header->section_table[section].media_end*0x200ULL - header->section_table[section].media_offset*0x200ULL;
}
