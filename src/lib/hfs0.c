#include "hfs0.h"

#include <stdlib.h>
#include <string.h>
#include <assert.h>

int_fast64_t hfs0_parse(bae_file_entry_t** output_, baecb_read input, void* cbdata, uint_fast64_t offset) {
	int_fast64_t ret;
	
	hfs0_header_t header;
	hfs0_file_entry_t* hfs_entry_table;
	char* hfs_stringtable;
	
	// Header
	(*input)(&header, offset, sizeof(hfs0_header_t), cbdata);
	ret = header.num_files;
	
	// Entry table
	hfs_entry_table = malloc( sizeof(hfs0_file_entry_t) * header.num_files );
	assert(hfs_entry_table != NULL);
	(*input)(hfs_entry_table, offset + 0x10, sizeof(hfs0_file_entry_t) * header.num_files, cbdata);
	
	// String table
	uint_fast64_t stringtable_offset = offset + 0x10 + sizeof(hfs0_file_entry_t)*header.num_files;
	hfs_stringtable = malloc( header.stringtable_size );
	assert(hfs_stringtable != NULL);
	(*input)(hfs_stringtable, stringtable_offset, header.stringtable_size, cbdata);
	
	uint_fast64_t data_offset = stringtable_offset + header.stringtable_size;
	
	*output_ = malloc( sizeof(bae_file_entry_t) * header.num_files );
	bae_file_entry_t* output = *output_;
	assert(output != NULL);
	
	for(uint32_t i=0; i<header.num_files; i++) {
		hfs0_file_entry_t* hfs_e = &hfs_entry_table[i];
		output[i].filename = strdup( &hfs_stringtable[ hfs_e->file_name_offset ] );
		assert(output[i].filename != NULL);
		output[i].offset = data_offset + hfs_e->file_offset;
		output[i].size = hfs_e->file_size;
	}
	
	free(hfs_stringtable);
	free(hfs_entry_table);
	
	return ret;
}
