#include "switchkeys.h"

#include "utils.h"

#include "minIni/dev/minIni.h"

#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>

#ifdef _WIN32
#define DEFAULT_KEYFILE "\\.switch\\prod.keys"
#else
#define DEFAULT_KEYFILE "/.switch/prod.keys"
#endif
#define DEFAULT_KEYFILE_NOHOME "/prod.keys"

#ifdef _WIN32
#define realpath(path, resolved_path) _fullpath(resolved_path, path, PATH_MAX)
#endif

switchkeys_t switchkeys;

// ishex(), hextoi(), parse_hex_key() taken from hactool © SciresM
static int ishex(char c) {
	if ('a' <= c && c <= 'f') return 1;
	if ('A' <= c && c <= 'F') return 1;
	if ('0' <= c && c <= '9') return 1;
	return 0;
}

static char hextoi(char c) {
	if ('a' <= c && c <= 'f') return c - 'a' + 0xA;
	if ('A' <= c && c <= 'F') return c - 'A' + 0xA;
	if ('0' <= c && c <= '9') return c - '0';
	return 0;
}

static bool parse_hex_key(unsigned char *key, const char *hex, unsigned int len) {
	if (strlen(hex) != 2 * len) {
		//fprintf(stderr, "Key (%s) must be %"PRIu32" hex digits!\n", hex, 2 * len);
		return false;
	}

	for (unsigned int i = 0; i < 2 * len; i++) {
		if (!ishex(hex[i])) {
			//fprintf(stderr, "Key (%s) must be %"PRIu32" hex digits!\n", hex, 2 * len);
			return false;
		}
	}

	memset(key, 0, len);

	for (unsigned int i = 0; i < 2 * len; i++) {
		char val = hextoi(hex[i]);
		if ((i & 1) == 0) {
			val <<= 4;
		}
		key[i >> 1] |= val;
	}
	return true;
}

static void import_key(char* keyfile, char* keyname, char* output, uint_fast8_t size)
{
	char key_octets[2*size+1]; // +1 for trailing null
	ini_gets("", keyname, "", key_octets, 2*size+1, keyfile);
	if(key_octets[0] == '\0') {
		// Keyfile does not have this key
		return;
	}
	
	parse_hex_key(output, key_octets, size);
}

void switchkeys_parse_titlekey(char* out, char* key) {
	parse_hex_key(out, key, 16);
}

char* switchkeys_default_keyfile_path() {
	char* rel_keyfile;
	// MinGW MSYS defines $HOME pointing to the MSYS folder, not the user's actual home directory.
#ifdef _WIN32
	char* homedir = getenv("USERPROFILE"); // Because Windows just *has* to be different.
#else
	char* homedir = getenv("HOME");
#endif
	
	if(homedir != NULL) {
		rel_keyfile = malloc( strlen(homedir) + strlen(DEFAULT_KEYFILE) + 1 );
		assert(rel_keyfile != NULL);
		strcpy(rel_keyfile, homedir);
		strcat(rel_keyfile, DEFAULT_KEYFILE);
	}
	else rel_keyfile = DEFAULT_KEYFILE_NOHOME;
	
	return rel_keyfile;
}

void switchkeys_load_keyfile(char* specified_keyfile) {
	char* keyfile = specified_keyfile;
	if(keyfile == NULL) {
		char* rel_keyfile = switchkeys_default_keyfile_path();
		keyfile = realpath(rel_keyfile, NULL);
		free(rel_keyfile);
	}
	if(keyfile == NULL)
		// TODO: Return error
		return;
	
	import_key(keyfile, "header_key", switchkeys.header, 32);
	
	for(unsigned int i=0; i<32; i++) {
		static char appkak_string[28] = "key_area_key_application_XX";
		sprintf(&appkak_string[25], "%02u", i);
		import_key(keyfile, appkak_string, switchkeys.application_key_area[i], 16);
		
		static char titlekek_string[12] = "titlekek_XX";
		sprintf(&titlekek_string[9], "%02u", i);
		import_key(keyfile, titlekek_string, switchkeys.titlekeks[i], 16);
	}
	
	free(keyfile);
}
