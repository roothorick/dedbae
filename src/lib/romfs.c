#include "romfs.h"

#include <limits.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>

#ifdef _WIN32
#include "internal_string.h"
#endif

#define TERMINATOR 0xFFFFFFFF

int_fast64_t romfs_parse(bae_file_entry_t** output_, baecb_read input, void* cbdata, uint_fast64_t fs_offset) {
	romfs_header_t header;
	bae_file_entry_t* output;
	int_fast64_t count = 0;
	char* dir_table_raw;
	char* file_table_raw;
	
	// NOTE: Nested function (GCC extension)
	void process_dir(uint32_t offset, char* path) {
		romfs_dir_entry_t* dir = (romfs_dir_entry_t*) (dir_table_raw + offset);
		
		uint32_t file_addr = dir->file;
		while(file_addr != TERMINATOR) {
			romfs_file_entry_t* file = (romfs_file_entry_t*) (file_table_raw + file_addr);
			
			bae_file_entry_t* outfile = &output[count];
			count++;
			
			outfile->filename = strndup(file->name, file->name_size);
			assert(outfile->filename != NULL);
			outfile->offset = fs_offset + header.data_offset + file->offset;
			outfile->size = file->size;
			
			file_addr = file->sibling;
		}
		
		if(dir->child != TERMINATOR) {
			char new_path[PATH_MAX];
			// TODO: Absurdly long directory names/trees could cause overflows
			strcpy(new_path, path);
			strncat(new_path, dir->name, dir->name_size);
			
			process_dir(dir->child, new_path);
		}
		
		if(dir->sibling != TERMINATOR)
			process_dir(dir->sibling, path);
	}
	
	// Read header
	(*input)(&header, fs_offset, sizeof(romfs_header_t), cbdata);
	
	dir_table_raw = malloc(header.dir_meta_table_size);
	assert(dir_table_raw != NULL);
	(*input)(dir_table_raw, fs_offset + header.dir_meta_table_offset, header.dir_meta_table_size, cbdata);
	
	file_table_raw = malloc(header.file_meta_table_size);
	assert(file_table_raw != NULL);
	(*input)(file_table_raw, fs_offset + header.file_meta_table_offset, header.file_meta_table_size, cbdata);
	
	// Each file entry is a minimum of 0x20 bytes. We don't know how many there are,
	// but we do know that there are less than file_meta_table_size / 0x20.
	// Greedy allocate so we don't have to make our own linked list.
	*output_ = malloc( header.file_meta_table_size / 0x20 * sizeof(bae_file_entry_t) );
	output = *output_;
	assert(output != NULL);
	
	process_dir(0, "");
	
	// Partial free now that we know how many files we actually have.
	*output_ = realloc(*output_, sizeof(bae_file_entry_t) * count);
	assert(*output_ != NULL);
	
	return count;
}
