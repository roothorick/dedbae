#include "baefiles.h"

#include <stdlib.h>

void bae_free_file_list(bae_file_entry_t* list, uint_fast32_t count) {
	for(uint_fast8_t i=0; i<count; i++)
			free(list[i].filename);

	free(list);
}
