#include "checks.h"

#include "switchkeys.h"

#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>

void check(bool condition, const char* message, ...) {
	if(!condition) {
		va_list args;
		va_start(args, message);
		vprintf(message, args);
		va_end(args);
// On Windows, hang so that the console window remains open
#ifdef _WIN32
		while(true) sleep(60);
#else
		exit(1);
#endif
	}
}

void* checked_malloc(size_t size) {
	void* ret = malloc(size);
	check(ret != NULL, "malloc() failed. Your computer might be melting.\n");
	return ret;
}

size_t checked_fread(void *ptr, size_t size, size_t nmemb, FILE *stream) {
	size_t ret = fread(ptr, size, nmemb, stream);
	if(ret != nmemb) {
		check(feof(stream) != 0, "Unexpected end-of-file. Corrupt file?\n");
		check(ferror(stream) != 0, "Error while reading: %s\n", strerror(errno) );
		check(false, "Unexpected short read. Disk or filesystem issue?\n");
	}
	return ret;
}

size_t checked_fwrite(const void *ptr, size_t size, size_t nmemb, FILE *stream) {
	size_t ret = fwrite(ptr, size, nmemb, stream);
	if(ret != nmemb) {
		check(ferror(stream) != 0, "Error while writing: %s\n", strerror(errno) );
		check(false, "Unexpected short write. Disk or filesystem issue?\n");
	}
	return ret;
}

int checked_fseeko(FILE *stream, off_t offset, int whence) {
	int ret = fseeko(stream, offset, whence);
	check(ret == 0, "Error while seeking: %s\n", strerror(errno) );
	return ret;
}

off_t checked_ftello(FILE *stream) {
	off_t ret = ftello(stream);
	check(ret != -1, "ftello() failed: %s\n", strerror(errno) );
	return ret;
}

void check_key(char* key, char* keyname) {
	// Currently, no known key begins with 0x00
	check(key[0] != 0x00,
			"Unable to load key %s!\n"
			"Make sure you have a valid key list at %s and it contains the header keys and all five application key area keys.\n",
			keyname, switchkeys_default_keyfile_path() ); // We don't care about the leak because this does not return
}
