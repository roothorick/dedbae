#include "xci.h"
#include "hfs0.h"
#include "pfs0.h"
#include "romfs.h"
#include "cnmt.h"
#include "nca.h"
#include "types.h"
#include "switchkeys.h"
#include "unidecode/unidecode.h"

#include "checks.h"

#define _FILE_OFFSET_BITS 64
#include <stdio.h>
#include <stdbool.h>
#include <unistd.h>
#include <stdlib.h>
#include <limits.h>
#include <stdarg.h>
#include <math.h>
#include <inttypes.h>

#ifndef NAME_MAX
#define NAME_MAX _MAX_FNAME
#endif

// Host limits (hopefully) do not matter. FAT32 and exFAT support 255 chars max.
#if NAME_MAX < 255
#define FAT_NAME_MAX NAME_MAX
#else
#define FAT_NAME_MAX 255
#endif

// Being a little cute -- writing out the file as cnmt_create_content_record() hashes it.
typedef struct {
	char* ncahdr;
	FILE* readfd;
	FILE* writefd;
	uint64_t read_offset;
	uint64_t write_offset;
} callback_for_nca_args;

uint_fast64_t callback_for_nca(void* buf, uint_fast64_t offset, uint_fast64_t size, void* data) {
	callback_for_nca_args* args = data;
	uint_fast64_t ret = 0;
	
	// First read from file
	checked_fseeko(args->readfd, offset + args->read_offset, SEEK_SET);
	ret = checked_fread(buf, 1, size, args->readfd);
	
	// If appropriate, overwrite the NCA header with the new one
	if( offset < sizeof(nca_header_t) ) {
		// Read from header
		if( offset + size > sizeof(nca_header_t) )
			memcpy(buf, args->ncahdr + offset, sizeof(nca_header_t) - offset);
		else
			memcpy(buf, args->ncahdr + offset, size);
	}
	
	// If desired, write to file
	if(args->writefd != NULL) {
		checked_fseeko(args->writefd, offset + args->write_offset, SEEK_SET);
		checked_fwrite(buf, 1, size, args->writefd);
	}
	
	return ret;
}

// fd doesn't necessarily have to be just the control NCA; it can be an XCI, seeked to the start of the control NCA
char* get_app_name(FILE* control_nca) {
	nca_header_t ncahdr;
	bae_file_entry_t* files;
	size_t start_of_file = checked_ftello(control_nca);
	
	// Load NCA header
	char enc_hdr[ sizeof(nca_header_t) ];
	checked_fread(enc_hdr, 1, sizeof(nca_header_t), control_nca);
	nca_decrypt_header(&ncahdr, enc_hdr);
	
	// Parse RomFS
	uint_fast64_t romfs_size = nca_sizeof_section(&ncahdr, 0);
	check(romfs_size < 0xa00000, // 10MiB
		"Absurdly large control RomFS. Corrupt file?\n");
	check( strncmp( ncahdr.section_headers[0].romfs.magic, "IVFC", 4 ) == 0 &&
			ncahdr.section_headers[0].romfs.magic2 == 0x20000,
			"Bad RomFS section header magic. Corrupt file?\n");
	
	// Read and decrypt entire section
	char* enc_romfs = checked_malloc(romfs_size);
	checked_fseeko(control_nca, start_of_file + nca_offsetof_section(&ncahdr, 0), SEEK_SET);
	checked_fread(enc_romfs, 1, romfs_size, control_nca);
	
	char* raw_romfs = checked_malloc(romfs_size);
	nca_decrypt_section(&ncahdr, 0, raw_romfs, enc_romfs, romfs_size, NULL);
	free(enc_romfs);
	
	int_fast64_t num_files = romfs_parse(&files, &baecb_read_memcpy, raw_romfs, ncahdr.section_headers[0].romfs.levels[5].offset);
	
	// There are 15 language sections. All are always present, but some may be filled with zeroes, so we might have to
	// hunt a little bit to find a usable title.
	char* rawname = NULL;
	for(int_fast64_t i=0; i<num_files; i++)
		if( strcmp(files[i].filename, "control.nacp") == 0 ) {
			for(uint_fast8_t j=0; j<15; j++)
				if( *( raw_romfs + files[i].offset + j*0x300 ) != '\0' ) {
					rawname = raw_romfs + files[i].offset + j*0x300;
					break;
				}
				
			break;
		}
	
	check(rawname != NULL, "control.nacp missing or invalid. Corrupt file?\n");
	
	char ret[FAT_NAME_MAX+1]; // w/Trailing null
	unidecode(rawname, ret, FAT_NAME_MAX);
	
	bae_free_file_list(files, num_files);
	free(raw_romfs);
	
	
	// Windows doesn't allow certain characters in file names
	char* forbidden_chars = "<>:\"/\\|?*";
	for(int i=0; i<strlen(forbidden_chars); i++) {
		char* badchar = strchr(ret, forbidden_chars[i]);
		while(badchar != NULL) {
			*badchar = ' '; // Space, not null
			// Restart search from where the bad char was found
			badchar = strchr(badchar, forbidden_chars[i]);
		}
	}
	
	return strdup(ret);
}

uint_fast64_t get_secure_partition_offset(FILE* xci_fd) {
	xci_header_t xci_header;
	
	bae_file_entry_t* partitions;
	int_fast64_t num_partitions;
	
	uint_fast64_t ret;

	// Parse header
	checked_fread(&xci_header, 1, sizeof(xci_header_t), xci_fd);
	check( strncmp(xci_header.magic, "HEAD", 4) == 0, "Invalid XCI header. Corrupt file or not an XCI?\n");
	
	// Parse master HFS0
	num_partitions = hfs0_parse(&partitions, &baecb_read_stdio, xci_fd, xci_header.hfs0_offset);
	
	// Find the secure partition
	int_fast64_t i = 0;
	while(true) {
		check(i < num_partitions, "No secure partition? This is very unexpected and you should tell me about it.\n");
		
		if( strcmp(partitions[i].filename, "secure") == 0 ) {
			ret = partitions[i].offset;
			break;
		}
		i++;
	}
	
	bae_free_file_list(partitions, num_partitions);
	return ret;
}

void output_rename(char* name) {
	rename(".unknown.nsp", name);
}

void process_xci(char* xci_name) {
	callback_for_nca_args cb_nca_args;
	
	FILE* xci_fd;
	
	bae_file_entry_t* input_files;
	int_fast64_t num_input_files;
	
	char nsp_name[FAT_NAME_MAX+1]; // w/Trailing null
	FILE* nsp_fd;
	pfs0_header_t nsp_header;
	pfs0_file_entry_t* nsp_entries;
	char* nsp_stringtable;
	uint64_t nsp_data_offset;
	
	printf("Processing %s...\n", xci_name);
	xci_fd = fopen(xci_name, "rb");
	check(xci_fd != NULL, "Failed to open XCI file.\n");
	
	uint_fast64_t securehfs_offset = get_secure_partition_offset(xci_fd);
	
	// Load Secure HFS0
	num_input_files = hfs0_parse( &input_files, &baecb_read_stdio, xci_fd, securehfs_offset );
	
	// Load header ourselves, for stringtable size
	hfs0_header_t securehfs_header;
	checked_fseeko(xci_fd, securehfs_offset, SEEK_SET);
	checked_fread(&securehfs_header, 1, sizeof(hfs0_header_t), xci_fd);
	
	// We don't yet know how many files we'll be including. Make space for all of them.
	nsp_entries = checked_malloc( sizeof(pfs0_file_entry_t) * num_input_files );
	memset( nsp_entries, 0, sizeof(pfs0_file_entry_t) * num_input_files );
	nsp_stringtable = checked_malloc(securehfs_header.stringtable_size);
	memset( nsp_stringtable, 0, securehfs_header.stringtable_size );
	
	// Prepare NSP header struct
	memset( &nsp_header, 0, sizeof(pfs0_header_t) );
	memcpy(nsp_header.magic, "PFS0", 4);
	
	// For each CNMT,
	for(uint_fast32_t i=0; i<num_input_files; i++) {

		if( strstr(input_files[i].filename, ".cnmt.nca") == NULL ) continue;
		
		char* old_cnmt_raw_nca;
		nca_header_t old_cnmt_nca_header;
		char* old_cnmt_nca_section;
		
		char* old_cnmt_pfs0;
		pfs0_header_t* old_cnmt_pfs0_header;
		pfs0_file_entry_t* old_cnmt_pfs0_entries;
		char* old_cnmt_pfs0_stringtable;
		
		char* old_cnmt;
		cnmt_header_t* old_cnmt_header;
		cnmt_content_record_t* old_cnmt_records;
		char* old_cnmt_digest;
		
		// 0x100000 == 1MiB
		check( input_files[i].size < 0x100000, "%s: Unreasonably large for a CNMT NCA. Corrupt file?\n", input_files[i].filename);
		
		checked_fseeko(xci_fd, input_files[i].offset, SEEK_SET);
		
		// Read the entire file -- it's small
		old_cnmt_raw_nca = checked_malloc(input_files[i].size);
		checked_fread(old_cnmt_raw_nca, 1, input_files[i].size, xci_fd);
		
		// Extract the PFS0 from the NCA
		nca_decrypt_header(&old_cnmt_nca_header, old_cnmt_raw_nca);
		check( strncmp(old_cnmt_nca_header.magic, "NCA3", 4) == 0, "%s: Invalid NCA header. Corrupt file?\n", input_files[i].filename);
		
		old_cnmt_nca_section = checked_malloc( nca_sizeof_section(&old_cnmt_nca_header, 0) );
		nca_decrypt_section(&old_cnmt_nca_header, 0, old_cnmt_nca_section,
					old_cnmt_raw_nca + nca_offsetof_section(&old_cnmt_nca_header, 0),
					nca_sizeof_section(&old_cnmt_nca_header, 0), NULL);
		old_cnmt_pfs0 = old_cnmt_nca_section + old_cnmt_nca_header.section_headers[0].pfs0.header_offset;
		
		// Set up convenience pointers
		old_cnmt_pfs0_header = (pfs0_header_t*) old_cnmt_pfs0;
		check( strncmp(old_cnmt_pfs0_header->magic, "PFS0", 4) == 0, "%s: Invalid PFS0 header. Corrupt file?\n");
		old_cnmt_pfs0_entries = (pfs0_file_entry_t*) ( old_cnmt_pfs0 + sizeof(pfs0_header_t) );
		old_cnmt_pfs0_stringtable = old_cnmt_pfs0 + sizeof(pfs0_header_t) + sizeof(pfs0_file_entry_t)*old_cnmt_pfs0_header->filecount;
		
		// Parse CNMT
		check( strstr( old_cnmt_pfs0_stringtable + old_cnmt_pfs0_entries[0].name_offset, ".cnmt" ) != NULL, "%s: Appears to contain non-CNMT data. Corrupt file?\n");
		old_cnmt = old_cnmt_pfs0_stringtable + old_cnmt_pfs0_header->stringtable_size + old_cnmt_pfs0_entries[0].offset;
		
		// Copy out data
		old_cnmt_header = (cnmt_header_t*) old_cnmt;
		
		uint64_t content_record_size_total = sizeof(cnmt_content_record_t) * old_cnmt_header->content_count;
		old_cnmt_records = (cnmt_content_record_t*) (old_cnmt + old_cnmt_header->table_offset + 0x20);
		old_cnmt_digest = old_cnmt + sizeof(cnmt_header_t) + content_record_size_total;
		
		memset(nsp_name, 0, FAT_NAME_MAX);
		nsp_header.filecount = 0;
		
		// Is this an update or the base game?
		if( old_cnmt_header->title_type == TITLE_TYPE_APP_UPDATE) {
			printf("Embedded update found. Creating update NSP...\n");
			// Package the unmodified NCAs and cert/tik/XML. They're already good to install.
			
			// Compiling the header, stringtable, and file entries
			
			// We'll save the in-XCI offsets of files, so we don't have to hunt for them a second time
			uint_fast64_t* offsets_in_xci = checked_malloc( sizeof(uint64_t) * securehfs_header.num_files );
			
			for(uint_fast64_t j=0; j<num_input_files; j++) {
				// Skip NCAs not mentioned in the CNMT (except the CNMT itself)
				if( i != j && strstr(input_files[j].filename, ".nca") != NULL) {
					uint_fast16_t k;
					// See if NCA is mentioned in CNMT
					for(k=0; k < old_cnmt_header->content_count; k++) {
						char ncaid[33]; // 32 hex digits + trailing null
						
						// Convert NCAID in CNMT to a partial string
						for(uint_fast8_t l=0; l<16; l++) {
							// This writes a null to every odd-numbered byte except the first, only to ovewrite it
							// immediately after (except the last). Probably not a significant performance hit.
							// Probably.
							sprintf(ncaid + l*2, "%02x", (unsigned char) old_cnmt_records[k].ncaid[l]);
						}
						
						if( strncmp(input_files[j].filename, ncaid, 32) == 0 ) {
							// A quick detour: If this is the control NCA, we want to get the app name.
							// Might as well compile the full file name while we're at it.
							if(old_cnmt_records[k].type == CNMT_CONTENT_TYPE_CONTROL) {
								checked_fseeko(xci_fd, input_files[j].offset, SEEK_SET);
								
								// Game name
								char* appname = get_app_name(xci_fd);
								
								// Title ID
								char titleid[17]; // 16 hex digits (8 bytes) + trailing null
								sprintf(titleid, "%016"PRIx64, old_cnmt_header->titleid);
								
								// Version
								char version[9]; // No more than 8 hex digits (4 bytes) + trailing null
								sprintf(version, "%x", old_cnmt_header->version);
								
								// Truncate title if going over filename limits
								uint_fast8_t suffix_length = 14 + // " [" + "][" + "][UPD].nsp"
										strlen(titleid) +
										strlen(version);
								
								if( strlen(appname) + suffix_length > FAT_NAME_MAX )
									appname[FAT_NAME_MAX - suffix_length] = '\0';
								
								sprintf(nsp_name, "%s [%s][%s][UPD].nsp", appname, titleid, version);
								
								free(appname);
							}
							
							break;
						}
					}
					if( k == old_cnmt_header->content_count ) continue;
				}
				// Non-NCA files will be included
				
				uint_fast32_t nsp_table_idx = nsp_header.filecount;
				
				// PFS0 entry
				nsp_entries[nsp_table_idx].filesize = input_files[j].size;
				nsp_entries[nsp_table_idx].name_offset = nsp_header.stringtable_size;
				
				// Stringtable
				strcpy(&nsp_stringtable[ nsp_header.stringtable_size ], input_files[j].filename);
				
				offsets_in_xci[nsp_table_idx] = input_files[j].offset;
				
				// Update PFS0 header
				nsp_header.filecount++;
				nsp_header.stringtable_size += strlen(input_files[j].filename)+1;
			}
			check(nsp_name[0] != '\0', "Update CNMT does not mention a control NCA. Corrupt file?\n");
			
			// We'll rename it later
			nsp_fd = fopen(".unknown.nsp", "wb");
			check(nsp_fd != NULL, "Failed to create output NSP.\n");
			
			// Write files to NSP
			checked_fseeko(nsp_fd,
					sizeof(pfs0_header_t) +
					sizeof(pfs0_file_entry_t)*nsp_header.filecount +
					nsp_header.stringtable_size,
					SEEK_SET);
			nsp_data_offset = 0;
			const uint_fast64_t BUFSIZE = 0x100000; // 1MiB
			char* buf = checked_malloc(BUFSIZE);
			for(uint_fast32_t j=0; j<nsp_header.filecount; j++) {
				checked_fseeko(xci_fd, offsets_in_xci[j], SEEK_SET);
				uint_fast64_t k = 0;
				for(; k+BUFSIZE < nsp_entries[j].filesize; k += BUFSIZE) {
					checked_fread(buf, 1, BUFSIZE, xci_fd);
					checked_fwrite(buf, 1, BUFSIZE, nsp_fd);
				}
				// Last less-than-1MB
				checked_fread(buf, 1, nsp_entries[j].filesize - k, xci_fd);
				checked_fwrite(buf, 1, nsp_entries[j].filesize - k, nsp_fd);
				
				nsp_entries[j].offset = nsp_data_offset;
				nsp_data_offset += nsp_entries[j].filesize;
			}
			free(buf);
			
			// Write NSP header, file entries and stringtable
			checked_fseeko(nsp_fd, 0, SEEK_SET);
			checked_fwrite(&nsp_header, 1, sizeof(pfs0_header_t), nsp_fd);
			checked_fwrite(nsp_entries, 1, sizeof(pfs0_file_entry_t) * nsp_header.filecount, nsp_fd);
			checked_fwrite(nsp_stringtable, 1, nsp_header.stringtable_size, nsp_fd);
			
			fclose(nsp_fd);
			output_rename(nsp_name);
			
			printf("Saved embedded update to %s\n", nsp_name);
			
			free(offsets_in_xci);
			
			continue;
		}
		
		printf("Creating application NSP...\n");
		// Rewrite the headers of each NCA and pack them up without a cert/tik/XML. Tinfoil doesn't care.
		
		cnmt_header_t new_cnmt_header;
		cnmt_init_header(&new_cnmt_header, old_cnmt_header);
		new_cnmt_header.content_count = old_cnmt_header->content_count;
		cnmt_content_record_t* new_cnmt_records = checked_malloc(sizeof(cnmt_content_record_t) * old_cnmt_header->content_count);
		
		// We'll be renaming it later
		nsp_fd = fopen(".unknown.nsp", "wb");
		check(nsp_fd != NULL, "Failed to create output NSP.\n");
		
		// So we're being a bit cute here.
		// We know exactly how many files there will be, and exactly what the length of each file name is.
		// This means we already know exactly how many content entries there will be, and exactly how big
		// the string table will be. Add it all together and you have the start of the data area.
		nsp_data_offset = 
				sizeof(pfs0_header_t) +
				sizeof(pfs0_file_entry_t) * (new_cnmt_header.content_count+1) + // CNMT doesn't mention itself
				42 + // <32 hex digits>.cnmt.nca + trailing null
				37 * new_cnmt_header.content_count; // <32 hex digits>.nca + trailing null, each
			
		checked_fseeko(nsp_fd, nsp_data_offset, SEEK_SET);
		
		uint_fast64_t datapos = 0;
		for(uint_fast16_t j=0; j<old_cnmt_header->content_count; j++) {
			char nca_name[37]; // <32 hex digits>.nca + trailing null
			
			
			// Convert NCAID in CNMT to a partial string
			for(uint_fast8_t k=0; k<16; k++) {
				snprintf(nca_name + k*2, 3, "%02x", (unsigned char) old_cnmt_records[j].ncaid[k]);
			}
			// Add extension
			sprintf(nca_name + 32, ".nca");
			
			// Find the NCA in the HFS0
			bae_file_entry_t* file = NULL;
			for(uint_fast64_t k=0; k < num_input_files; k++) {
				if( strcmp( nca_name, input_files[k].filename ) == 0 ) {
					file = &input_files[k];
					break;
				}
			}
			check(file != NULL, "Couldn't find %s in HFS0 secure partition. Corrupt file?\n", nca_name);
			
			// If it's the control NCA, take a moment to compile the NSP name
			if(old_cnmt_records[j].type == CNMT_CONTENT_TYPE_CONTROL) {
				checked_fseeko(xci_fd, file->offset, SEEK_SET);
				
				// Game name
				char* appname = get_app_name(xci_fd);
				
				// Title ID
				char titleid[17]; // 16 hex digits (8 bytes) + trailing null
				sprintf(titleid, "%016"PRIx64, old_cnmt_header->titleid);
				
				// Version
				char version[9]; // No more than 8 hex digits (4 bytes) + trailing null
				sprintf(version, "%x", old_cnmt_header->version);
				
				// Truncate title if going over filename limits
				uint_fast8_t suffix_length = 9 + // " [" + "][" + "].nsp"
						strlen(titleid) +
						strlen(version);
				
				if( strlen(appname) + suffix_length > FAT_NAME_MAX )
					appname[FAT_NAME_MAX - suffix_length] = '\0';
				
				sprintf(nsp_name, "%s [%s][%s].nsp", appname, titleid, version);
				
				free(appname);
			}
			
			checked_fseeko(xci_fd, file->offset, SEEK_SET);
			
			// Rewrite header
			nca_header_t ncahdr;
			char enc_hdr[ sizeof(nca_header_t) ];
			checked_fread(enc_hdr, 1, sizeof(nca_header_t), xci_fd);
			
			nca_decrypt_header(&ncahdr, enc_hdr);
			check( strncmp(ncahdr.magic, "NCA3", 4) == 0, "%s: Invalid NCA header. Corrupt file?\n" );
			ncahdr.location = NCA_LOCATION_CONSOLE;
			nca_encrypt_header(enc_hdr, &ncahdr);
			
			// Prepare loaded read callback
			cb_nca_args.ncahdr = enc_hdr;
			cb_nca_args.readfd = xci_fd;
			cb_nca_args.writefd = nsp_fd;
			cb_nca_args.read_offset = file->offset;
			cb_nca_args.write_offset = checked_ftello(nsp_fd);
			
			// Create CNMT record, simultaneously writing the file to the NSP via the callback
			cnmt_create_content_record(&new_cnmt_records[j], &callback_for_nca, &cb_nca_args, file->size);
			
			// Copy content type from old CNMT (NCA content type flag works a bit differently/incompatible)
			new_cnmt_records[j].type = old_cnmt_records[j].type;
			
			// Assemble new filename
			char new_filename[37]; // <32 hex digits> + ".nca" + trailing null
			for(uint_fast8_t k=0; k<16; k++)
				sprintf(new_filename + k*2, "%02x", (unsigned char) new_cnmt_records[j].ncaid[k]);
			
			strcat(new_filename, ".nca");
			
			// Add to stringtable
			strcpy(&nsp_stringtable[nsp_header.stringtable_size], new_filename);
			
			// File table entry
			nsp_entries[j].offset = datapos;
			nsp_entries[j].filesize = file->size;
			nsp_entries[j].name_offset = nsp_header.stringtable_size;
			
			// Update header
			nsp_header.filecount++;
			nsp_header.stringtable_size += strlen(file->filename)+1;
			
			datapos += file->size;
		}
		check(nsp_name[0] != '\0', "Application CNMT does not list a control NCA. Corrupt file?\n");
		
		const size_t CNMT_FILENAME_LEN = 34; // "Application_" + <16 hex digits> + ".cnmt" + trailing null
		 char cnmt_filename[CNMT_FILENAME_LEN];
		sprintf(cnmt_filename, "Application_%016"PRIx64".cnmt", new_cnmt_header.titleid);
		
		uint_fast64_t new_cnmt_pfs0_size =
				sizeof(pfs0_header_t) +
				sizeof(pfs0_file_entry_t) +
				CNMT_FILENAME_LEN + // CNMT file name (PFS0 stringtable)
				sizeof(cnmt_header_t) +
				sizeof(cnmt_content_record_t) * new_cnmt_header.content_count + 32; // Digest
		char* new_cnmt_pfs0 = checked_malloc(new_cnmt_pfs0_size);
		
		uint_fast64_t filepos = 0;
		// PFS0 header
		pfs0_header_t new_cnmt_pfs0_header = {
			.magic = { 'P', 'F', 'S', '0' }, // This avoids the trailing null
			.filecount = 1,
			.stringtable_size = CNMT_FILENAME_LEN, // cnmt_filename
		};
		memcpy(new_cnmt_pfs0, &new_cnmt_pfs0_header, sizeof(pfs0_header_t) );
		filepos += sizeof(pfs0_header_t);
		
		// The singular PFS0 file entry
		pfs0_file_entry_t new_cnmt_pfs0_entry = {
			.offset = 0,
			.filesize = sizeof(cnmt_header_t) + sizeof(cnmt_content_record_t) * new_cnmt_header.content_count + 32, // digest at the end
			.name_offset = 0,
		};
		memcpy(new_cnmt_pfs0 + filepos, &new_cnmt_pfs0_entry, sizeof(pfs0_file_entry_t) );
		filepos += sizeof(pfs0_file_entry_t);
		
		// File name (PFS0 stringtable)
		memcpy(new_cnmt_pfs0 + filepos, cnmt_filename, CNMT_FILENAME_LEN);
		filepos += CNMT_FILENAME_LEN;
		
		// CNMT header
		memcpy(new_cnmt_pfs0 + filepos, &new_cnmt_header, sizeof(cnmt_header_t) );
		filepos += sizeof(cnmt_header_t);
		
		// CNMT content records
		memcpy(new_cnmt_pfs0 + filepos, new_cnmt_records, sizeof(cnmt_content_record_t) * new_cnmt_header.content_count);
		filepos += sizeof(cnmt_content_record_t) * new_cnmt_header.content_count;
		
		// Digest
		memcpy(new_cnmt_pfs0 + filepos, old_cnmt_digest, 32);
		
		// The complete CNMT + PFS0 is now in new_cnmt_pfs0
		
		// XXX: This should be done by the library... somehow. *How* is nontrivial to figure out.
		// 0xc00 NCA header + hashtable (one hash) + CNMT PFS0 + padding to make it even 0x200 blocks
		uint_fast64_t new_cnmt_nca_size = 0xc00 + 0x20 + ceil(new_cnmt_pfs0_size / (float) 0x200) * 0x200;
		char* new_cnmt_nca = checked_malloc(new_cnmt_nca_size);
		
		nca_header_t new_cnmt_nca_header;
		nca_init_header(&new_cnmt_nca_header, &old_cnmt_nca_header);
		new_cnmt_nca_header.location = NCA_LOCATION_CONSOLE;
		nca_inject_pfs0(&new_cnmt_nca_header, 0, &baecb_write_memcpy, new_cnmt_nca, &baecb_read_memcpy, new_cnmt_pfs0, new_cnmt_pfs0_size, NULL);
		nca_encrypt_header(new_cnmt_nca, &new_cnmt_nca_header);
		
		// CNMT's file name
		const uint_fast8_t NEW_CNMT_NCA_NAME_LENGTH = 42; // 32 hex digits + ".cnmt.nca" + trailing null
		char new_cnmt_nca_name[NEW_CNMT_NCA_NAME_LENGTH];
		nca_generate_ncaid(new_cnmt_nca_name, &baecb_read_memcpy, new_cnmt_nca, new_cnmt_nca_size);
		strcat(new_cnmt_nca_name, ".cnmt.nca");
		
		// Write CNMT to NSP
		checked_fwrite(new_cnmt_nca, 1, new_cnmt_nca_size, nsp_fd);
		
		// Add CNMT NCA to NSP metadata
		strcpy(&nsp_stringtable[nsp_header.stringtable_size], new_cnmt_nca_name);
		
		uint_fast32_t idx = nsp_header.filecount;
		nsp_entries[idx].offset = datapos;
		nsp_entries[idx].filesize = new_cnmt_nca_size;
		nsp_entries[idx].name_offset = nsp_header.stringtable_size;
		
		nsp_header.filecount++;
		nsp_header.stringtable_size += NEW_CNMT_NCA_NAME_LENGTH;
		
		// Write out NSP PFS0 stuff
		checked_fseeko(nsp_fd, 0, SEEK_SET);
		checked_fwrite(&nsp_header, 1, sizeof(pfs0_header_t), nsp_fd);
		checked_fwrite(nsp_entries, 1, sizeof(pfs0_file_entry_t) * nsp_header.filecount, nsp_fd);
		checked_fwrite(nsp_stringtable, 1, nsp_header.stringtable_size, nsp_fd);
		
		fclose(nsp_fd);
		output_rename(nsp_name);
		
		printf("Saved application to %s\n", nsp_name);
		
		free(new_cnmt_nca);
		free(new_cnmt_pfs0);
		free(new_cnmt_records);
		free(old_cnmt_nca_section);
		free(old_cnmt_raw_nca);
	}
	
	fclose(xci_fd);

	free(nsp_stringtable);
	free(nsp_entries);
	printf("\n");
}

int main(int argc, char* argv[]) {
	printf("dedbae XCI to NSP conversion tool (c) roothorick. Under ISC license, see LICENSE file for details.\n");
	printf("\n");
	
	switchkeys_load_keyfile(NULL);
	
	check_key(switchkeys.header, "header_key");
	check_key(switchkeys.application_key_area[0], "key_area_key_application_00");
	check_key(switchkeys.application_key_area[1], "key_area_key_application_01");
	check_key(switchkeys.application_key_area[2], "key_area_key_application_02");
	check_key(switchkeys.application_key_area[3], "key_area_key_application_03");
	check_key(switchkeys.application_key_area[4], "key_area_key_application_04");
	
	check(argc >= 2, "Usage: xci2nsp <xci> [xci] [...]\n"
	"Or just drag and drop XCI files onto the executable.\n");
	
	for(int i=1; i<argc; i++)
		process_xci(argv[i]);
	
	printf("Done!\n");
}
