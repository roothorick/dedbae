#include "actions.h"

#include "xci.h"
#include "hfs0.h"
#include "utils.h"
#include "fast_copy.h"

#define _FILE_OFFSET_BITS 64
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>

void act_extractxci(char* file) {
	FILE* fd = fopen(file, "rb");
	
	xci_header_t header;
	fread(&header, 1, sizeof(xci_header_t), fd);

	bae_file_entry_t* partitions;
	int_fast64_t num_partitions = hfs0_parse(&partitions, &baecb_read_stdio, fd, header.hfs0_offset);
	
	for(uint_fast8_t i=0; i<num_partitions; i++) {
		bae_file_entry_t* part = &partitions[i];
#ifdef _WIN32
		int ret = mkdir(part->filename);
#else
		int ret = mkdir(part->filename, 0777);
#endif
		if(ret != 0)
			bail("Failed to create directory");
		chdir(part->filename);
		
		bae_file_entry_t* files;
		int_fast64_t num_files = hfs0_parse(&files, &baecb_read_stdio, fd, part->offset);
		
		for(uint_fast64_t j=0; j<num_files; j++) {
			bae_file_entry_t* f = &files[j];
			FILE* outfd = fopen(f->filename, "wb");
			
			// Set up for sendfile
			char nullbyte = '\0';
			fseeko(outfd, f->size - 1, SEEK_SET);
			fwrite(&nullbyte, 1, 1, outfd);
			fseeko(outfd, 0, SEEK_SET);
			
			fast_copy(outfd, fd, f->offset, f->size);
			
			fclose(outfd);
		}
		chdir("..");
		
		bae_free_file_list(files, num_files);
	}

	bae_free_file_list(partitions, num_partitions);

	fclose(fd);
}
