#include "actions.h"

#include "romfs.h"

#define _FILE_OFFSET_BITS 64
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>

romfs_header_t romfs_header;
char* dir_table_raw;
char* file_table_raw;

void print_tabs(uint_fast8_t num) {
	for(uint_fast8_t i=0; i<num; i++)
		printf("\t");
}

void act_printromfs(char* file) {
	
	FILE* fd = fopen(file, "rb");
	
	bae_file_entry_t* files;
	int_fast64_t num_files = romfs_parse(&files, &baecb_read_stdio, fd, 0);
	
	fclose(fd);
	
	for(uint_fast64_t i=0; i<num_files; i++) {
		printf( "%s: Offset %lx size %lx\n", files[i].filename, files[i].offset, files[i].size );
	}
	
	bae_free_file_list(files, num_files);
}
