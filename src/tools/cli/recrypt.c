#include "actions.h"
#include "nca.h"
#include "utils.h"
#include "args.h"
#include "fast_copy.h"

#include <string.h>
#include <unistd.h>
#define _FILE_OFFSET_BITS 64
#include <stdio.h>

#define SCRATCHFILENAME ".dedbae.scratch"

void act_recrypt(char* infilename, char* outfilename) {
	nca_header_t old_hdr, new_hdr;
	char enc_hdr[ sizeof(nca_header_t) ];
	
	FILE* infile = fopen( infilename, "rb" );
	if(infile == NULL)
		bail("Failed to open input file");
	
	FILE* outfile = fopen( outfilename, "wb" );
	if(outfile == NULL)
		bail("Failed to open output file");
	
	fread(enc_hdr, 1, sizeof(nca_header_t), infile);
	nca_decrypt_header(&old_hdr, enc_hdr);
	memcpy( &new_hdr, &old_hdr, sizeof(nca_header_t) );
	
	if(cmdline_args.location != NULL) {
		if( strcmp(cmdline_args.location, "console") == 0 )
			new_hdr.location = NCA_LOCATION_CONSOLE;
		else
			new_hdr.location = NCA_LOCATION_GAMECARD;
	}
	
	if(cmdline_args.content != NULL) {
		if( strcmp(cmdline_args.content, "program") == 0 )
			new_hdr.content_type = NCA_CONTENT_TYPE_PROGRAM;
		else if( strcmp(cmdline_args.content, "meta") == 0)
			new_hdr.content_type = NCA_CONTENT_TYPE_META;
		else if( strcmp(cmdline_args.content, "control") == 0)
			new_hdr.content_type = NCA_CONTENT_TYPE_CONTROL;
		else if( strcmp(cmdline_args.content, "manual")  == 0)
			new_hdr.content_type = NCA_CONTENT_TYPE_MANUAL;
		else
			new_hdr.content_type = NCA_CONTENT_TYPE_DATA;
	}
	
	memset(new_hdr.rights_id, 0x00, 16);
	if(cmdline_args.out_titlekey != NULL) {
		// In all observed official cases, the Rights ID is the title ID padded with zeroes,
		// with the value of the NCA crypto_type2 field appended.
		// TitleID is stored as an LSB integer, but RightsID is a byte array
		char* titleid_bytes = (char*) &old_hdr.titleid;
		for(int i=0; i<8; i++) {
			int j=7-i;
			new_hdr.rights_id[i] = titleid_bytes[j];
		}
		new_hdr.rights_id[15] = new_hdr.crypto_type2;
	}
	
	for(uint_fast8_t i=0; i<4; i++) {
		if( old_hdr.section_table[i].media_offset == 0x0 )
			// No more sections
			break;
			
		if(cmdline_args.crypto[i] != 0)
			new_hdr.section_headers[i].cryptotype = cmdline_args.crypto[i];
		
		else if( old_hdr.section_headers[i].cryptotype == NCA_SECTION_CRYPTO_TYPE_PLAINTEXT ) {
			// Copy directly into the output without changes
			fseeko(infile, nca_offsetof_section(&old_hdr, i), SEEK_SET);
			fseeko(outfile, nca_offsetof_section(&new_hdr, i), SEEK_SET);
			
			uint_fast64_t section_size = nca_sizeof_section(&old_hdr, i);
			
			// Sendfile requires the write area to already be within the bounds of the file
			const char nul = '\0';
			fseeko(outfile, nca_offsetof_section(&new_hdr, i) + section_size - 1, SEEK_SET);
			fwrite(&nul, 1, 1, outfile);
			fseeko(outfile, nca_offsetof_section(&new_hdr, i), SEEK_SET);
			
			fast_copy(outfile, infile, ftello(infile), section_size);
			
			continue;
		}
		
		// Decrypt to a scratch file
		FILE* scratch = fopen(SCRATCHFILENAME, "wb+");
		nca_extract_section(&old_hdr, i,
				&baecb_write_stdio, scratch,
				&baecb_read_stdio, infile, cmdline_args.titlekey);
		
		// Encrypt to new file
		nca_replace_section(&new_hdr, i,
				&baecb_write_stdio, outfile,
				&baecb_read_stdio, scratch, cmdline_args.out_titlekey);
		fclose(scratch);
		unlink(SCRATCHFILENAME);
	}
	
	nca_encrypt_header(enc_hdr, &new_hdr);
	fseeko(outfile, 0, SEEK_SET);
	fwrite(enc_hdr, 1, sizeof(nca_header_t), outfile);
	
	fclose(outfile);
	fclose(infile);
}
