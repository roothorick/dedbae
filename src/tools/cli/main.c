#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#define _FILE_OFFSET_BITS 64
#include <stdio.h>

#include "args.h"
#include "actions.h"
#include "utils.h"
#include "switchkeys.h"
#include "types.h"
#include "nca.h"

#ifndef NAME_MAX
#define NAME_MAX PATH_MAX
#endif

cmdline_args_t cmdline_args;

void header(void) {
	printf(
		"dedbae CLI tool (c) roothorick. Under ISC license, see LICENSE file for details.\n"
		"\n"
	);
}

void usage() {
	// Commented out lines are TODO
	printf(
		"Usage: dbcli [options] <action>\n"
		"\n"
		"Actions:\n"
		"recrypt <input.nca> <output.nca>\n"
		"        (Re-)encrypt NCA with specified changes\n"
		"gencnmt <cnmt> <nca1> [nca2] [...]\n"
		"        Generate a CNMT describing these NCAs\n"
		"printcnmt <cnmt>\n"
		"        Pretty print the information in a CNMT\n"
		"create <nca> <section0> [section1..3]\n"
		"        Create an NCA from scratch\n"
		"xciinfo <xci>\n"
		"        Print information about an XCI file\n"
		//"extractxci <xci>\n"
		//"        Extract XCI contents\n"
		"printromfs <romfs>\n"
		"        Print contents of a RomFS file\n"
		"\n"
		"Options:\n"
		"--oldfile <file>\n"
		"        Use values from this file instead of the defaults\n"
		"--keyfile <path>\n"
		"        Use specified keyfile (default: ~/.switch/prod.keys)\n"
		"--titlekey <key>\n"
		"        Use this titlekey for decryption (for titlekey crypto)\n"
		"--out-titlekey <key>\n"
		"        Encrypt using titlekey crypto, with this key\n"
		"--titleid <id>\n"
		"        Set/change title ID\n"
		"--crypto <section>:<plain|ctr>[,<section>:<plain|ctr>,...]\n"
		"        Change the crypto type of a section (default: ctr)\n"
		"--content <program|meta|control|manual|data>\n"
		"        Change the NCA content type (default: meta if gencnmt, otherwise program)\n"
		"--location <console|gamecard>\n"
		"        Change/specify the NCA location flag (default: console)\n"
		"--titletype <type>\n"
		"        For gencnmt, title type (default: application)\n"
		"        Valid title types: sysapp sysdata sysupdate firmware_a\n"
		"                           firmware_b application appupdate dlc delta\n"
		"\n"
	);
}

// DOES NOT RETURN
void invalid_cmdline(char* message) {
	printf("%s\n\n", message);
	usage();
	exit(1);
}

char make_sections_bitmap(char* sections_in) {
  char ret = 0x0;
  
	for(uint_fast8_t i=0; i<strlen(sections_in); i++) {
		switch( sections_in[i] ) {
			case '0':
				ret |= 0x1; // bit 0
				break;
			case '1':
				ret |= 0x2; // bit 1
				break;
			case '2':
				ret |= 0x4; // bit 2
				break;
			case '3':
				ret |= 0x8; // bit 3
				break;
			default:
				invalid_cmdline("Invalid section specifier. Must be any combination of: 0 1 2 3");
		}
	}
	
	return ret;
}

int main(int argc, char* argv[]) {
	header();
	
	const struct option CMDLINE_OPTIONS[] = {
			{ "keyfile", 1, NULL, 1 },
			{ "titlekey", 1, NULL, 2 },
			{ "titleid", 1, NULL, 3 },
			{ "out-titlekey", 1, NULL, 4 },
			{ "crypto", 1, NULL, 5 },
			{ "location", 1, NULL, 6 },
			{ "digest", 1, NULL, 7 },
			{ "xml", 0, NULL, 8 },
			{ "apptype", 1, NULL, 9 },
			{ "oldfile", 1, NULL, 10 },
			{ "content", 1, NULL, 11 },
			{ NULL, 0, NULL, 0 }
	};
	
	memset(&cmdline_args, 0, sizeof(cmdline_args_t));
	
	while(1) {
		
		int opt_ret = getopt_long(argc, argv, "", CMDLINE_OPTIONS, NULL);
		
		if(opt_ret == -1) break;
		
		switch( opt_ret ) {
			case '?':
				// getopt_long() already printed an error
				printf("\n");
				usage();
				return 1;
				break;
			case 1: // keyfile
				cmdline_args.keyfile = optarg;
				break;
			case 2: // titlekey
				if( strlen(optarg) != 32 )
					invalid_cmdline("Titlekey is wrong length.");
				cmdline_args.titlekey = malloc(16);
				switchkeys_parse_titlekey(cmdline_args.titlekey, optarg);
				break;
			case 3: // titleid
				if( strlen(optarg) != 16 )
					invalid_cmdline("TitleID is wrong length.");
				cmdline_args.titleid = optarg;
				break;
			case 4: // out_titlekey
				if( strlen(optarg) != 32 )
					invalid_cmdline("Output titlekey is wrong length.");
				cmdline_args.out_titlekey = malloc(16);
				switchkeys_parse_titlekey(cmdline_args.out_titlekey, optarg);
				break;
			case 5: // crypto
				((void)0); // Otherwise GCC tries to parse the charptr decl as part of the case label
				char* idx = optarg;
				while(true) {
					char* n_idx;
					unsigned int section = strtoul(idx, &n_idx, 10);
					if(section > 3 || idx == n_idx || *n_idx != ':')
						invalid_cmdline("Invalid crypto type specifier.");
					
					idx = n_idx+1;
					if( strncmp(idx, "plain", 5) == 0 ) {
						cmdline_args.crypto[section] = NCA_SECTION_CRYPTO_TYPE_PLAINTEXT;
						idx += strlen("plain");
					}
					else if( strncmp(idx, "ctr", 3) == 0 ) {
						cmdline_args.crypto[section] = NCA_SECTION_CRYPTO_TYPE_CTR;
						idx += strlen("ctr");
					}
					else
						invalid_cmdline("Invalid crypto type specifier.");
					
					if(*idx == '\0')
						break;
					else if(*idx != ',')
						invalid_cmdline("Invalid crypto type specifier.");
					
					idx++;
				}
				break;
			case 6: // location
				if( strcmp(optarg, "console") == 0 || strcmp(optarg, "gamecard") == 0 )
					cmdline_args.location = optarg;
				else
					invalid_cmdline("Invalid location");
				break;
			case 7: // digest
				if( strlen(optarg) != 32 )
					invalid_cmdline("Digest is wrong length.");
				cmdline_args.digest = optarg;
				break;
			case 8: // xml
				cmdline_args.xml = true;
				break;
			case 9: // titletype
				if( strcmp(optarg, "sysapp") == 0 )
					cmdline_args.titletype = TITLE_TYPE_SYSTEM_PROGRAM;
				else if( strcmp(optarg, "sysdata") == 0 )
					cmdline_args.titletype = TITLE_TYPE_SYSTEM_DATA;
				else if( strcmp(optarg, "sysupdate") == 0 )
					cmdline_args.titletype = TITLE_TYPE_SYSTEM_UPDATE;
				else if( strcmp(optarg, "firmware_a") == 0 )
					cmdline_args.titletype = TITLE_TYPE_FIRMWARE_A;
				else if( strcmp(optarg, "firmware_b") == 0 )
					cmdline_args.titletype = TITLE_TYPE_FIRMWARE_B;
				else if( strcmp(optarg, "application") == 0 )
					cmdline_args.titletype = TITLE_TYPE_APPLICATION;
				else if( strcmp(optarg, "appupdate") == 0 )
					cmdline_args.titletype = TITLE_TYPE_APP_UPDATE;
				else if( strcmp(optarg, "dlc") == 0 )
					cmdline_args.titletype = TITLE_TYPE_ADDON_CONTENT;
				else if( strcmp(optarg, "delta") == 0 )
					cmdline_args.titletype = TITLE_TYPE_DELTA;
				else
					invalid_cmdline("Invalid title type");
			case 10: // oldfile
				cmdline_args.oldfile = optarg;
				break;
			case 11: //content
				if( strcmp(optarg, "program") != 0 &&
						strcmp(optarg, "meta") != 0 &&
						strcmp(optarg, "control") != 0 &&
						strcmp(optarg, "manual") != 0 &&
						strcmp(optarg, "data") != 0 )
					invalid_cmdline("Invalid content type");
				cmdline_args.content = optarg;
		}
	}
	
	switchkeys_load_keyfile(cmdline_args.keyfile);
	
	if( optind >= argc )
		invalid_cmdline("No action specified.");
	
	char* command = argv[optind];
	char* files[5];
	uint_fast8_t num_files;
	int_fast8_t j = -1;
	memset(files, 0, sizeof(char*) * 5);
	// The command is at optind+0, so the first file is at optind+1
	for(uint_fast8_t i=optind+1; i<argc; i++) {
		j = i-optind-1;
		if(j>=6)
			invalid_cmdline("Too many files.");
		files[j] = argv[i];
	}
	num_files = j+1;
	
	if( strcmp(command, "recrypt") == 0 ) {
		if( num_files < 1 )
			invalid_cmdline("No input file specified.");
		else if( num_files < 2 )
			invalid_cmdline("No output file specified.");
		else
			act_recrypt(files[0], files[1]);
	}
	else if( strcmp(command, "create") == 0 ) {
		if( num_files < 1 )
			invalid_cmdline("No output file specified.");
		else if( num_files < 2 )
			invalid_cmdline("No input files specified.");
		else if( num_files > 5 )
			invalid_cmdline("Too many sections. NCAs can have at most four sections.");
		else
			act_create(files[0], files + 1, num_files - 1);
	}
	else if( strcmp(command, "gencnmt") == 0 ) {
		if( num_files < 1 )
			invalid_cmdline("No output file specified.");
		else if( num_files < 2 )
			invalid_cmdline("No NCAs specified");
		else
			act_gencnmt(files[0], files + 1, num_files - 1);
	}
	else if( strcmp(command, "printcnmt") == 0 ) {
		if( num_files < 1 )
			invalid_cmdline("No file specified.");
		else if( num_files > 1)
			invalid_cmdline("Too many files on commandline");
		else
			act_printcnmt(files[0]);
	}
	else if( strcmp(command, "xciinfo") == 0 ) {
		if( num_files < 1 )
			invalid_cmdline("No file specified.");
		else if( num_files > 1)
			invalid_cmdline("Too many files on commandline");
		else
			act_xciinfo(files[0]);
	}
/*	else if( strcmp(command, "extractxci") == 0 ) {
		if( num_files < 1 )
			invalid_cmdline("No file specified.");
		else if( num_files > 1)
			invalid_cmdline("Too many files on commandline");
		else
			act_extractxci(files[0]);
	}*/
	else if( strcmp(command, "printromfs") == 0 ) {
		if( num_files < 1 )
			invalid_cmdline("No file specified.");
		else if( num_files > 1)
			invalid_cmdline("Too many files on commandline");
		else
			act_printromfs(files[0]);
	}
	else
		invalid_cmdline("Invalid action specified");
	
	printf("\n");
	printf("All done!\n");
	return 0;
}
