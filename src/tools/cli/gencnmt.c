#include "actions.h"

#include "cnmt.h"
#include "types.h"
#include "nca.h"
#include "utils.h"
#include "args.h"

#include <stdlib.h>
#define _FILE_OFFSET_BITS 64
#include <stdio.h>

void act_gencnmt(char* outfilename, char* nca_filenames[], uint_fast16_t num_files) {
	cnmt_header_t cnmt_header;
	cnmt_content_record_t* content_records;
	char digest[32];
	
	memset(&cnmt_header, 0, sizeof(cnmt_header_t));
	if( cmdline_args.digest != NULL )
		memcpy(digest, cmdline_args.digest, 32);
	else if( cmdline_args.oldfile != NULL ) {
		FILE* oldfile = fopen(cmdline_args.oldfile, "rb");
		
		cnmt_header_t old_header;
		fread(&old_header, 1, sizeof(cnmt_header_t), oldfile);
		cnmt_init_header(&cnmt_header, &old_header);
		
		fseeko(oldfile, -32, SEEK_END);
		fread(digest, 1, 32, oldfile);
		fclose(oldfile);
	}
	else {
		cnmt_init_header(&cnmt_header, NULL);
		memset(digest, 0, 32);
	}
	
	if( cmdline_args.titleid != NULL ) {
		cnmt_header.titleid = strtoull(cmdline_args.titleid, NULL, 16);
	}
	
	if( cmdline_args.titletype != 0 )
		cnmt_header.title_type = cmdline_args.titletype;
	
	content_records = malloc(sizeof(cnmt_content_record_t) * num_files);
	memset(content_records, 0, sizeof(cnmt_content_record_t) * num_files);
	
	for(uint_fast16_t i=0; i<num_files; i++) {
		FILE* nca = fopen(nca_filenames[i], "rb");
		if(nca == NULL)
			bail("Failed to open input file");
		
		// Extract title ID from the first NCA
		if(i==0 && cmdline_args.titleid == NULL) {
			char enc_hdr[ sizeof(nca_header_t) ];
			fread(enc_hdr, 1, sizeof(nca_header_t), nca);
			nca_header_t hdr;
			nca_decrypt_header(&hdr, enc_hdr);
			cnmt_header.titleid = hdr.titleid;
		}
		
		fseeko(nca, 0, SEEK_END);
		uint_fast64_t filesize = ftello(nca);
		fseeko(nca, 0, SEEK_SET);
		
		printf("Generating content record for %s...", nca_filenames[i]);
		cnmt_create_content_record(&content_records[i], &baecb_read_stdio, nca, filesize);
		printf("Done!\n");
		
		fclose(nca);
	}
	
	cnmt_header.content_count = num_files;
	
	printf("Writing CNMT...");
	FILE* outfile = fopen(outfilename, "wb+");
	if(outfile == NULL)
		bail("Failed to open output file");
	fwrite(&cnmt_header, 1, sizeof(cnmt_header_t), outfile);

	
	for(uint_fast16_t i=0; i<num_files; i++)
		fwrite(&content_records[i], 1, sizeof(cnmt_content_record_t), outfile);
	
	fwrite(digest, 1, 32, outfile);
	
	fclose(outfile);
	printf("Done!\n");
	
	free(content_records);
}
