#pragma once

#define _FILE_OFFSET_BITS 64
#include <stdio.h>

#ifdef __linux__
#include <sys/sendfile.h>
#endif

static inline uint_fast64_t fast_copy(FILE* out, FILE* in, uint_fast64_t offset, uint_fast64_t count) {
	uint_fast64_t writtensize = 0;
#ifdef __linux__
	off_t cur_offset = offset;
	while(writtensize < count)
		writtensize += sendfile( fileno(out), fileno(in), &cur_offset, count - writtensize);

	return writtensize;
#else
// TODO: Can we abuse mmap to induce a kenel-level copy?
	const uint_fast64_t BUFSIZE = 0x10000; // 64KiB
	uint_fast64_t next_read = BUFSIZE;
	char buf[BUFSIZE];
	do {
		if(writtensize + BUFSIZE < count)
			next_read = count - writtensize;
		uint_fast64_t part_readsize = 0;
		while(part_readsize < next_read)
			part_readsize += fread(buf + part_readsize, 1, next_read - part_readsize,  in);
		uint_fast64_t part_writtensize = 0;
		while(part_writtensize < next_read)
			part_writtensize += fwrite(buf + part_writtensize, 1, next_read - part_writtensize, out);
		writtensize += next_read;
	} while(next_read == BUFSIZE);
#endif
	
	return writtensize;
}
