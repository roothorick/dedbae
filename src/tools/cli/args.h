#pragma once

#include <stdint.h>
#include <stdbool.h>

typedef struct {
	char* keyfile;
	char* titlekey;
	char* titleid;
	char* out_titlekey;
	uint8_t crypto[4];
	char* location; // 0 is a valid value; just point to the argv string
	char* digest;
	bool xml;
	uint8_t titletype;
	char* oldfile;
	char* content; // same thing, 0 is valid
} cmdline_args_t;

extern cmdline_args_t cmdline_args; // main.c
