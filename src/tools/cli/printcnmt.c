#include "actions.h"

#include "cnmt.h"
#include "types.h"
#include "utils.h"

#define _FILE_OFFSET_BITS 64
#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>

void print_hex(char* array, unsigned int num) {
	for(unsigned int i=0; i<num; i++)
		printf("%02x", (unsigned char) array[i]);
}

void act_printcnmt(char* file) {
	cnmt_header_t header;
	
	FILE* fd = fopen(file, "rb");
	if( fd == NULL )
		bail("Failed to open file!");
	
	size_t readsize = fread(&header, 1, sizeof(cnmt_header_t), fd);
	if( readsize != sizeof(cnmt_header_t) )
		bail("Failed to read header!");
	
	printf("Title ID     : %016"PRIx64"\n", header.titleid);
	printf("Title version: %x\n", header.version);
	printf("Title type   : %s\n", title_type_to_string(header.title_type));
	printf("Table offset : 0x%x\n", header.table_offset + 0x20);
	
	if( header.title_type == TITLE_TYPE_ADDON_CONTENT )
		printf("Minimum application version: ");
	else
		printf("Minimum system version: ");
	
	char* sysversion = cnmt_minversion_to_string(&header);
	printf("%s\n", sysversion);
	free(sysversion);
	
	printf("\n");
	
	// XXX: Is it allowed for a CNMT to contain both content AND meta records?
	if(header.content_count > 0 && header.meta_count > 0)
		bail("CNMTs with both content and meta entries are not supported");
	
	fseeko(fd, header.table_offset + 0x20, SEEK_SET);
	
	if(header.content_count > 0) {
		printf("Number of content records: %u\n", header.content_count);
		for(uint_fast16_t i=0; i<header.content_count; i++) {
			cnmt_content_record_t rec;
			readsize = fread(&rec, 1, sizeof(cnmt_content_record_t), fd);
			if( readsize != sizeof(cnmt_content_record_t) )
				bail("Failed to read content record");
				
			printf("Content %lu: NCAID ", i); print_hex(rec.ncaid, 16);
				printf(" Type %s Size %lu bytes\n", cnmt_content_type_to_string(rec.type), cnmt_read_contentsize(&rec) );
			
			printf("    SHA256 hash: "); print_hex(rec.hash, 32); printf("\n");
			printf("\n");
		}
		
		
	}
	else {
		printf("Number of meta records: %u\n", header.meta_count);
		for(uint_fast16_t i=0; i<header.meta_count; i++) {
			cnmt_meta_record_t rec;
			readsize = fread(&rec, 1, sizeof(cnmt_meta_record_t), fd);
			if( readsize != sizeof(cnmt_meta_record_t) )
				bail("Failed to read meta record");
			
			printf( "Meta %lu: titleID %016lx Type %s\n", i, rec.titleid, title_type_to_string(rec.type) );
			
			printf("\n");
		}
	}
	
	char digest[32];
	readsize = fread(digest, 1, 32, fd);
	if(readsize < 32)
		bail("Failed to read digest");
	printf("CNMT Digest: "); print_hex(digest, 32); printf("\n");

	fclose(fd);
}
