#pragma once

#include "baecb.h"
#include "baefiles.h"

#include <stdint.h>

// romfs_header_t, romfs_dir_entry_t, romfs_file_entry_t adapted from hactool © SciresM
typedef struct __attribute__((packed)) {
	uint64_t header_size;
	uint64_t dir_hash_table_offset;
	uint64_t dir_hash_table_size;
	uint64_t dir_meta_table_offset;
	uint64_t dir_meta_table_size;
	uint64_t file_hash_table_offset;
	uint64_t file_hash_table_size;
	uint64_t file_meta_table_offset;
	uint64_t file_meta_table_size;
	uint64_t data_offset;
} romfs_header_t;

typedef struct __attribute__((packed)) {
	uint32_t parent;
	uint32_t sibling;
	uint32_t child;
	uint32_t file;
	uint32_t hash;
	uint32_t name_size;
	char name[];
} romfs_dir_entry_t;

typedef struct __attribute__((packed)) {
	uint32_t parent;
	uint32_t sibling;
	uint64_t offset;
	uint64_t size;
	uint32_t hash;
	uint32_t name_size;
	char name[];
} romfs_file_entry_t;

// Parse a RomFS into a file list
// Returns number of files in filesystem, or a negative error code on failure
int_fast64_t romfs_parse(bae_file_entry_t** output_, baecb_read input, void* cbdata, uint_fast64_t offset);
