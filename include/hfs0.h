#pragma once

#include "baecb.h"
#include "baefiles.h"

typedef struct __attribute__((packed)) {
	/* 0x 0 */ char magic[4]; // "HFS0"
	/* 0x 4 */ uint32_t num_files;
	/* 0x 8 */ uint32_t stringtable_size;
	/* 0x C */ char unknown_0[4]; // Zeroes?
} hfs0_header_t;
// File entry table starts at 0x10

typedef struct __attribute__((packed)) {
	/* 0x 0 */ uint64_t file_offset;
	/* 0x 8 */ uint64_t file_size;
	/* 0x10 */ uint32_t file_name_offset; // In string table
	/* 0x14 */ uint32_t hashed_region_size;
	/* 0x18 */ char unknown[8]; // zeroes?
	/* 0x20 */ char file_hash[0x20]; // Hash of only the first hashed_region_size bytes of the file
} hfs0_file_entry_t;
// String table starts immediately after the last file entry.
// The data area starts immediately after that.

// Parse an HFS0 into a file list
// Returns number of files in filesystem, or a negative error code on failure
int_fast64_t hfs0_parse(bae_file_entry_t** output_, baecb_read input, void* cbdata, uint_fast64_t offset);
