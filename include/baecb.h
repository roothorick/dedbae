#pragma once

#include <stdint.h>

// Callback used by some CNMT and NCA functions when the work area is too large for a simple memory buffer.
// These will be called with the buffer to read/write, at what offset in the file, how many bytes to read, and a
// pointer value of your choosing.
// Return the number of bytes actually read/written.
typedef uint_fast64_t (*baecb_read)(void* buf, uint_fast64_t offset, uint_fast64_t size, void* data);
typedef uint_fast64_t (*baecb_write)(void* buf, uint_fast64_t offset, uint_fast64_t size, void* data);

// Standard callback wrappers for memcpy(), when reading a buffer out of memory.
// Pass the input/output buffer for the data.
uint_fast64_t baecb_read_memcpy(void* buf, uint_fast64_t offset, uint_fast64_t size, void* data);
uint_fast64_t baecb_write_memcpy(void* buf, uint_fast64_t offset, uint_fast64_t size, void* data);

#ifndef DEDBAE_NO_STDIO_CALLBACKS
// Standard callback wrappers for stdio fseek()/fread()/fwrite(), for reading from a standard file.
// Pass a file handle from fopen() for the data.
uint_fast64_t baecb_read_stdio(void* buf, uint_fast64_t offset, uint_fast64_t size, void* data);
uint_fast64_t baecb_write_stdio(void* buf, uint_fast64_t offset, uint_fast64_t size, void* data);
#endif
